# macaConverter

class MXC_GLOBAL_CONFIGURATION:
    def __init__(self) -> None:
        self.NAME            = 'MACA Converter Tool'
        self.VERSION         = '1.0.3'
        self.LAST_MODIFIED   = '2023-5-31 10:33'

MXC_CONFIG = MXC_GLOBAL_CONFIGURATION()